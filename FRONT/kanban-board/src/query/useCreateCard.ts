import { useMutation, useQueryClient } from "@tanstack/react-query";
import { request } from "utils/request";
import { CardInfoProps } from "utils/interfaces";

const createCardRequest = async ({
  titulo,
  conteudo,
  lista,
}: CardInfoProps) => {
  const { data } = await request({
    url: "/cards",
    method: "POST",
    data: {
      titulo: titulo,
      conteudo: conteudo,
      lista: lista,
    },
  });

  return data;
};

const useCreateCard = () => {
  const queryClient = useQueryClient();
  return useMutation(createCardRequest, {
    onSuccess: (res) => {
      console.log("Success on create a new card", res);
      queryClient.setQueryData(["cardList"], (prevState: any) =>
        prevState ? prevState.concat(res) : prevState
      );
    },
    onError: (err) => {
      console.log("Error on create a new card", err);
    },
  });
};

export default useCreateCard;
