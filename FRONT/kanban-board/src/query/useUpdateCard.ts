import { useMutation, useQueryClient } from "@tanstack/react-query";
import { request } from "utils/request";
import { CardInfoProps } from "utils/interfaces";

const cardInfoUpdate = async (props: CardInfoProps) => {
  const { data } = await request({
    url: `/cards/${props.id}`,
    method: "PUT",
    data: { ...props },
  });

  return data;
};

const useUpdateCard = () => {
  const queryClient = useQueryClient();
  return useMutation(cardInfoUpdate, {
    onSuccess: (res, props) => {
      console.log("Success on update card", res);
      queryClient.setQueryData(["cardList"], (prevState: any) =>
        prevState
          ? prevState
              .filter((item: CardInfoProps) => item.id !== props.id)
              .concat(res)
          : prevState
      );
    },
    onError: (err) => console.log("Error on update card", err),
  });
};

export default useUpdateCard;
