import { useQuery } from "@tanstack/react-query";
import { request } from "../utils/request";
import { getFromLocalStorage } from "../utils/storage";
import { jwtToken } from "../utils/constants";
import { CardInfoProps } from "utils/interfaces";

const token = getFromLocalStorage(jwtToken);

const cardListRequest = async () => {
  const { data } = await request({
    url: "/cards",
    method: "GET",
  });

  return data;
};

const useCardList = () => {
  return useQuery<CardInfoProps[], Error>(
    ["cardList"],
    () => cardListRequest(),
    {
      enabled: !!token,
      refetchOnMount: 'always',
      refetchOnWindowFocus: true,
    }
  );
};

export default useCardList;
