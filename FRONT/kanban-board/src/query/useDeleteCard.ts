import { useMutation, useQueryClient } from "@tanstack/react-query";
import { request } from "utils/request";

const deleteCardById = async ({ id }: { id: string }) => {
  const { data } = await request({
    url: `/cards/${id}`,
    method: "DELETE",
  });

  return data;
};

const useDeleteCard = () => {
  const queryClient = useQueryClient();
  return useMutation(deleteCardById, {
    onSuccess: (res) => {
      console.log("Success on delete card", res);
      queryClient.setQueryData(["cardList"], (prevState: any) =>
        prevState ? res : prevState
      );
    },
    onError: (err) => console.log("Error on delete card", err),
  });
};

export default useDeleteCard;
