import React, { useEffect } from "react";
import useCardList from "query/useCardList";
import useCreateCard from "query/useCreateCard";
import useUpdateCard from "query/useUpdateCard";
import useDeleteCard from "query/useDeleteCard";

import { CardInfoProps, KanbanColumns } from "utils/interfaces";
// import { Container } from './styles';
import CardInfo from "components/Card";

import { Box, Flex, Heading } from "@chakra-ui/react";

const Board: React.FC = () => {
  const { data, isSuccess, isLoading, refetch } = useCardList();
  const mutationCreate = useCreateCard();
  const mutationUpdate = useUpdateCard();
  const mutationDelete = useDeleteCard();


  const createNewCard = ({
    titulo,
    conteudo,
    lista = "TO DO",
  }: CardInfoProps) => {
    mutationCreate.mutate({
      titulo: titulo,
      conteudo: conteudo,
      lista: lista,
    });
  };

  const updateCardById = ( props: CardInfoProps) => {
    mutationUpdate.mutate({...props});
  };

  const deleteCardById = (id: string) => {
    mutationDelete.mutate({ id });
  };

  useEffect(() => {
    console.log("card list", data);
    refetch();
  }, [isSuccess, isLoading, data]);

  return (
    <Box>
      <Flex direction="column" alignItems="center">
        <Heading size="3xl" color="#2d2d34">
          KANBAN
        </Heading>
        <Flex
          width="100%"
          direction="row"
          alignItems="flex-start"
          justifyContent="space-around"
          mt="20px"
        >
          <Flex flex="1" direction="column" alignItems="center">
            <Heading size="xl" color="#2d2d34" mb="10px">
              Novo
            </Heading>
            <CardInfo type="create" onCreate={createNewCard} />
          </Flex>
          <Flex flex="1" direction="column" alignItems="center">
            <Heading size="xl" color="#2d2d34" mb="10px">
              To Do
            </Heading>
            <Box maxHeight="600px" scrollBehavior="auto">
              {data
                ?.filter((item) => item.lista === KanbanColumns.TODO)
                .map((card: CardInfoProps, index) => (
                  <Box key={index} mb="20px">
                    <CardInfo
                      {...card}
                      type="moving"
                      onUpdate={updateCardById}
                      onDelete={deleteCardById}
                    />
                  </Box>
                ))}
            </Box>
          </Flex>
          <Flex flex="1" direction="column" alignItems="center">
            <Heading size="xl" color="#2d2d34" mb="10px">
              Doing
            </Heading>
            {data
              ?.filter((item) => item.lista === KanbanColumns.DOING)
              .map((card: CardInfoProps) => (
                <Box mb="20px">
                  <CardInfo
                    {...card}
                    type="moving"
                    onUpdate={updateCardById}
                    onDelete={deleteCardById}
                  />
                </Box>
              ))}
          </Flex>
          <Flex flex="1" direction="column" alignItems="center">
            <Heading size="xl" color="#2d2d34" mb="10px">
              Done
            </Heading>
            {data
              ?.filter((item) => item.lista === KanbanColumns.DONE)
              .map((card: CardInfoProps) => (
                <Box mb="20px">
                  <CardInfo
                    {...card}
                    type="moving"
                    onUpdate={updateCardById}
                    onDelete={deleteCardById}
                  />
                </Box>
              ))}
          </Flex>
        </Flex>
      </Flex>
    </Box>
  );
};

export default Board;
