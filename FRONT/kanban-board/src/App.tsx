import React, { useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ChakraProvider } from "@chakra-ui/react";
import { getToken } from "./utils/request";
import { getFromLocalStorage } from "./utils/storage";
import { jwtToken } from "./utils/constants";

import Board from "./screen/Board";

function App() {
  const queryClient = new QueryClient();
  const token = getFromLocalStorage(jwtToken);

  useEffect(() => {
    if (token === null) {
      getToken();
    }
  }, []);

  return (
    <QueryClientProvider client={queryClient}>
      <ChakraProvider>
        <Board />
      </ChakraProvider>
    </QueryClientProvider>
  );
}

export default App;
