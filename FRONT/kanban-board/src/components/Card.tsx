import React, { useState } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Input,
  Textarea,
  Flex,
  Button,
  IconButton,
  Heading,
  Box,
  Text,
} from "@chakra-ui/react";
import {
  AddIcon,
  ArrowForwardIcon,
  ArrowBackIcon,
  DeleteIcon,
  CloseIcon,
  EditIcon,
  CheckIcon,
} from "@chakra-ui/icons";
// import { Container } from './styles';
import { CardInfoProps, KanbanColumns } from "utils/interfaces";
interface CardInfoComponentProp extends CardInfoProps {
  type?: "create" | "editable" | "moving";
  onDelete?: (id: string) => void;
  onCreate?: (props: CardInfoProps) => void;
  onUpdate?: (props: CardInfoProps) => void;
  moveForward?: () => void;
  moveBack?: () => void;
}

const CardInfo: React.FC<CardInfoComponentProp> = ({
  type = "editable",
  titulo = "",
  conteudo = "",
  lista = "",
  id = "",
  onCreate,
  onUpdate,
  onDelete,
}) => {
  const [titleCard, setTitleCard] = useState(titulo);
  const [descriptionCard, setDescriptionCard] = useState(conteudo);
  const [typeMode, setTypeMode] = useState(type);

  const MovingForwardReference = (lista: string): string => {
    switch (lista) {
      case KanbanColumns.TODO:
        return KanbanColumns.DOING;
      case KanbanColumns.DOING:
        return KanbanColumns.DONE;
      default:
        return lista;
    }
  };

  const MovingBackwardReference = (lista: string): string => {
    switch (lista) {
      case KanbanColumns.DOING:
        return KanbanColumns.TODO;
      case KanbanColumns.DONE:
        return KanbanColumns.DOING;
      default:
        return lista;
    }
  };

  const handleCreateNewCard = () =>
    !!onCreate &&
    onCreate({
      titulo: titleCard,
      conteudo: descriptionCard,
      lista: KanbanColumns.TODO,
    });

  const handleMoveCardForward = () =>
    !!onUpdate &&
    onUpdate({ titulo, conteudo, lista: MovingForwardReference(lista), id });

  const handleMoveCardBackward = () =>
    !!onUpdate &&
    onUpdate({ titulo, conteudo, lista: MovingBackwardReference(lista), id });

  const handleDeleteCard = () => !!onDelete && onDelete(id);

  const handleEditMode = () =>
    typeMode === "editable" ? setTypeMode(type) : setTypeMode("editable");

  const handleEditCard = () => {
    !!onUpdate &&
      onUpdate({
        titulo: titleCard,
        conteudo: descriptionCard,
        lista: lista,
        id: id,
      });
    handleEditMode();
  };

  return (
    <Card variant="elevated">
      <CardHeader pb="5px">
        <Flex direction="column" alignItems="center">
          <Flex alignSelf="flex-end" mb="10px">
            {typeMode !== "editable" && typeMode !== "create" && (
              <IconButton
                aria-label="arrow forward"
                backgroundColor="#e2dcde"
                icon={<EditIcon />}
                onClick={handleEditMode}
              />
            )}
          </Flex>
          {typeMode === "editable" || typeMode === "create" ? (
            <Input
              placeholder="Título"
              onChange={(e) => setTitleCard(e.target.value)}
              value={titleCard}
            />
          ) : (
            <Heading size="lg" color="#555562">
              {titulo}
            </Heading>
          )}
        </Flex>
      </CardHeader>
      <CardBody pt="5px">
        {typeMode === "editable" || typeMode === "create" ? (
          <Textarea
            placeholder="Descrição"
            onChange={(e) => setDescriptionCard(e.target.value)}
            value={descriptionCard}
          />
        ) : (
          <Box
            border="0.5px solid #555562"
            borderRadius="5px"
            minWidth="250px"
            minHeight="120px"
            p='5px'
          >
            <Text>{conteudo}</Text>
          </Box>
        )}

        {type === "create" && (
          <Flex direction="row" justify="center" mt="10px">
            <Button
              fontSize="12px"
              backgroundColor="#e2dcde"
              leftIcon={<AddIcon />}
              onClick={handleCreateNewCard}
            >
              {" "}
              Criar{" "}
            </Button>
          </Flex>
        )}
        {typeMode === "editable" && (
          <Flex direction="row" justify="space-around" mt="10px">
            <Button
              fontSize="12px"
              backgroundColor="#e2dcde"
              leftIcon={<CloseIcon />}
              onClick={handleEditMode}
            >
              Cancelar
            </Button>
            <Button
              fontSize="12px"
              backgroundColor="#e2dcde"
              leftIcon={<CheckIcon />}
              onClick={handleEditCard}
            >
              Editar
            </Button>
          </Flex>
        )}
        {typeMode === "moving" && (
          <Flex direction="row" justify="space-around" mt="10px">
            <IconButton
              aria-label="arrow back"
              backgroundColor="#e2dcde"
              icon={<ArrowBackIcon />}
              onClick={handleMoveCardBackward}
            />
            <IconButton
              aria-label="delete"
              backgroundColor="#b97375"
              icon={<DeleteIcon />}
              onClick={handleDeleteCard}
            />
            <IconButton
              aria-label="arrow forward"
              backgroundColor="#e2dcde"
              icon={<ArrowForwardIcon />}
              onClick={handleMoveCardForward}
            />
          </Flex>
        )}
      </CardBody>
    </Card>
  );
};

export default CardInfo;
