import axios, { AxiosRequestConfig } from "axios";
import { isExpired } from 'react-jwt'
import { baseURL, jwtToken } from "./constants";
import { postOnLocalStorage, getFromLocalStorage } from "./storage";

const api = axios.create({
  timeout: 6000,
  baseURL: baseURL,
});

export const getToken = async () => {
  await api
    .post("/login", {
      login: "letscode",
      senha: "lets@123",
    })
    .then((response) => {
      console.log(response);
      postOnLocalStorage(jwtToken, response.data);
    })
    .catch((err) => {
      console.log("error: ", err);
    });
};

axios.interceptors.request.use(async function (config) {
  const token = getFromLocalStorage(jwtToken);
  if (token !== null) {
    if(isExpired(token)){
      try{
        postOnLocalStorage(jwtToken, '');
        await getToken();

        const refreshedToken = getFromLocalStorage(jwtToken);
        config.headers = {
          Authorization: `Bearer ${refreshedToken}`
        }
      } catch(err) {
        console.log('Error on refresh token', err);
      }
    }

  }
  return config;
});

export const request = async (config: AxiosRequestConfig) => {
    if(config.method === 'GET'){
        config.data = '';
    };

    const token = getFromLocalStorage(jwtToken);

    if(token !== null){
        config.headers = {
            Authorization: `Bearer ${token}`
        }
    }

    return await api.request(config);
}
