export interface CardInfoProps {
  id?: string;
  titulo?: string;
  conteudo?: string;
  lista?: string;
}

export const KanbanColumns = { TODO: "TO DO", DOING: "DOING", DONE: "DONE" };
