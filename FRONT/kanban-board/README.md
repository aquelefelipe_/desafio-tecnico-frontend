# Passo a passo para o projeto funcionar:

### Ir na pasta desafio-tecnico-frontend/BACK e executar os seguintes comandos:

1. yarn
2. yarn run server

ou

1. npm install
2. npm run server

### Após o passo anterior, ir para a pasta desafio-tecnico-frontend/FRONT/kanban-board e executar os seguintes comandos:

1. yarn
2. yarn start

ou

1. npm install
2. npm start

OBS: após a finalização do carregamento o projeto deverá esta funcionando em `http://localhost:3000` no seu browser.

### Observações

Minha dificuldade agora no finalzinho foi fazer o refresh token, tentei algumas abordagens mas nenhuma delas pareceu ter funcionado e como meu tempo estava limitado não consegui fazer essa `feature`
